resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-files-elias-py"
  force_destroy = true
}
